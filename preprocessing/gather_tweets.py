import pandas as pd
import sys

# get all keywords to search for relevant tweets
words ="opioid,heroin,EndTheStigma,stopMATstigma,opioidcrisis,AddictionReovery,AccessMATers,RecoveryNow,RxAwareness,breakthestigma,PatientsNoPrisoners,buprenorphine,MedicalKidnap,stopmedicalkidnapping,EndTheDrugWar,TreatmentNotTragedy,MAT,needleexchange,harmreduction,syringe,Narcan,Naloxone,overdose,opioidepidemic,opioidcrisis,addictionrecovery,drugaddiction,drugrehab,addiction,addictiontreatment,aspartame,Belbuca,benzodiazepines,booze,bupe,Buprenex,Buprenorphine,cannabis,cocaine,compulsions,depressant,Desoxyn,Diskets,Drugabuse,drugaddiction,drugcrimes,drugdeath,druginjection,drugpoisoning,drugpolicy,drugtoxicity,EndOverdose,Evzio,fentanyl,freenarcan,heroin,Intensol,Ket,ketamine,marijuana,mentalhealth,Methadone,Methadose,methamphetamine,naloxone,Narcan,narcansaveslives,Narcotic,nicotine,opiate,opiatesepidemic,opioidepidemic,opioidhysteria,opioidusedisorder,overdose,OverdoseAwareness,oxycontin,painpatients,Probuphine,PWID,Recovery programs,RecoveryPosse,specialk,stopoverdoses,SubstanceUseDisorder,weed"
keywords = words.split(",")

def get_tweets(file_name):
	df = pd.read_json(file_name)
	df2 = pd.DataFrame()
	for index, row in df.iterrows():
		tweet_words = row['message'].split(" ")
		count = 0				# count number of keywords present in tweet
		flag = False
		for i in range (len(keywords)):
			if keywords[i] in tweet_words:
				# print("Word found: ", keywords[i])
				count += 1
				flag = True
			if flag:
				row_df = pd.DataFrame([pd.Series([row['message'], row['message_id']])])
				df2 = pd.concat([row_df, df2], ignore_index=True)
				# print("Number of keywords found in current tweet: ", count)
	return df2

def main():
	rel_tweets = get_tweets(sys.argv[1])
	name = sys.argv[1][:8] + 'relevant_tweets.csv'
	rel_tweets.to_csv(name)

if __name__ == "__main__":
	main()
