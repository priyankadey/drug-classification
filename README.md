## The Motivation & Problem ##

Every year, thousands of Americans are found dead due to drug overdoses. If there was a way to determine those who are using, perhaps addiction/rehab facilities can help them avoid such overdoses. 
Often times, members of a community prefer to talk about their emotions through a virtual environment such as through social networking sites where they can also virtually meet others dealing with similar problems. Thus looking at social media posts (such as Twitter tweets) could give us an insight to drug users. The task then becomes: how can we determine whether a given tweet is about drug use?
This repository aims to solve this problem by finding a method to classify tweets about drugs into different categories.

## Data: ##
* MongoDB
	* Database: loc_messages, messages
	* collection: 2015_loc_messages, messages_2016_12 
* Command for extracting data from mongodb: mongoexport --collection=messages_2016_12 --db=messages --out=2016_12.json
* Preprocessing for db data is in: preprocessing.py
* Tweets were annotated manually and are taken from 2015 and a portion of 2016_12 (annotated: ~250 messages)
* Tweets for each category are stored in data/ and in the corresponding file 
* Other files were used as a preprocessing step for the naive bayes classifier 

## Tweet categories: ##
* News/research-related:
	These tweets discuss any events related to drug use
* Reactions/Opinions: 
	Often, tweets respond to a certain event or make some claim about drugs. For example, they have a negative reaction to someone smoking or comment on one's own beliefs about drugs. These tweets are generally categorized based on the amount of emotional/feelings present in the tweet or relation to opinions about a drug-related topic. 
* Comparison: 
	Often tweets refer to drugs to make some type of comparison with other objects (e.g. “whiter than the cocaine in my socks”)
* Abuse witnesses: 
	These tweets refer to someone commenting on the fact that they have/are witnessed/witnessing someone in person using 
* Users: 
	These tweets refer to those who either use or those who may have used in the past and/or are planning on using in the future. Tweets in this group can be further classified into 3 groups:
	* Possible intent to use: These tweets refer to those who are currently thinking of using but are not currently using at the moment. These tweets include words such as “want to” or “need to” showing that there may be possible usage in the future.
	* Current Usage: These tweets refer to those who are currently using (at the time the tweet was written). These tweets also include those that have an implication that the tweeter uses.
	* Past Usage: These tweets refer to events where the user has used in the past. Tweets in this category either discuss past events or suggest a strong possibility that the user has used in the past.
* Reactions: 
	Many tweets react to the drug crisis and state their opinions to usage and drugs in general. All such tweets have been classified until this broad term.

## Details on categories: ##

Tweet Category | Example Tweet | Comments on Classification | Total Count 
--- | --- | --- | --- |--- |--- |--- |--- |--- |--- |--- |---
News/research-related | “Monmouth County Officials: 14 Arrested In Heroin Ring Bust: A heroin ring has been busted and 14 people are under” | NA | 183 
Comparison | “Cant tell if thats Kermit or some good ass weed” | NA | 18
Abuse witnesses | “Cousin is doing cocaine right in front of me ????” | NA | 59
Possible intent to use | “Ill find some weed tonight.” | NA | 172
Current usage | “we only do cocaine on Saturdays bc work flow” | Although this tweet does not directly state that cocaine is being consumed at the moment, it is evident that the user and friend[s] use Saturdays. Thus, I have grouped sufficiently evident usage patterns under the term “current usage”. | 110
Past usage | “I told my mom I do methamphetamine and she responded with ""So thats why your face is so fucked up" | NA | 104
Reactions | “in debate we had to choose from 4 topics and i chose marijuana. Im debating on the CON side because marijuana is SO stupid.” | NA | 191

### Naive Bayes Classifier ###

* As a baseline classifier, I decided to use a naive bayes classifier
* Test 1 (April 2nd testing):
	* I chose to build a classifier for the users categories: possible intent to use, current usage, and past usage (since these have a larger number of tweets annotated for these categories as opposed to most others)
	* Treating each tweet as a document, I generated a TDM (term document matrix) and saved to the corresponding _tdm.csv
	* Annotated data was splitted 80%-20% (training/testing)
	* Overall accuracy: 0.40
	* Category accuracies (calculated by checking how many annotated tweets in this category were correctly classified):
		* Possible intent to use: 0.2
		* Current usage: 0.8
		* Past usage: 0.375

* Test 2 (April 7th testing):
	* There is now more data in each category, so I tried running the classification on all 7 groups:
	* Annotated data was split using 80-20 scheme as before
	* Overall accuracy: 0.44936708860759494

* Test 3 (April 8th testing):
	* There is not enough comparison tweet data, so let's try to test without that category
	* Same annotation as previous
	* Overall accuracy: 0.461038961038961
	
* Test 4 (April 8th testing):
	* Seeing if there is any improvement with 4 most important categories (past usage, current usage, possible intent to use, and news/research):
	* Same annotation as previous
	* Overall accuracy: 0.4375

### Next steps ###

* 2 different baseline classifiers? 
	* Naive bayes (done)
	* SVM? 
* Look into the bayesian classifier models in scikit-learn
* Explore other classifiers including: neural network and bayesian logistic regression

### TO-DO: some things to get done by Friday ###
* Need to put up the code for preprocessing the data
* Need to calculate the accuracy, precision, recall, and F-score (but doesn't look too good) -> maybe too many categories and too few tweet data?
* Need to work on the paper
* Need to update the documentation with new category values