
import os, sys, inspect

#TODO: temp fix, figure out how to include other dirs??
#sys.path.insert(0, '/home/squirtle/Documents/UIUC/projects/mentoring/tf-stis')
curr_dir = '/'.join(os.path.dirname(os.path.realpath(__file__)).split('/')[:-1])
sys.path.insert(0, curr_dir)
print(curr_dir)

# realpath() will make your script run, even if you symlink it :)
cmd_folder = os.path.realpath(os.path.abspath(os.path.split(inspect.getfile(inspect.currentframe()))[0]))
if cmd_folder not in sys.path:
    sys.path.insert(0, cmd_folder)

# Use this if you want to include modules from a subfolder
cmd_subfolder = os.path.realpath(
    os.path.abspath(os.path.join(os.path.split(inspect.getfile(inspect.currentframe()))[0], "subfolder")))
if cmd_subfolder not in sys.path:
    sys.path.insert(0, cmd_subfolder)

#from tffeature_ct import TFFeature
# from utils_ct import preprocess_text
from sklearn.neural_network import MLPClassifier
from sklearn.ensemble import RandomForestClassifier
from sklearn.ensemble import ExtraTreesClassifier
from sklearn.naive_bayes import MultinomialNB
from sklearn.model_selection import GridSearchCV
from sklearn.metrics import f1_score, classification_report
from sklearn.svm import SVC
from sklearn.linear_model import LogisticRegression
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfVectorizer

from scipy.sparse import hstack

from nltk.corpus import stopwords

import pandas as pd
import numpy as np

import pickle
import argparse
import dill
import json

from util import makedirs
from util import tokenize_tweets
#from twokenize import tokenizeRawTweetText, normalizeTextForTagger, tokenize
#from CMUTweetTagger import runtagger_parse

RANDOMSEED=420369


class BOWClassifier:

    def __init__(self, model='forest', verbose=True, output_file = 'tempoutputfile.txt', coarse_labels=False, class_weight='balanced',
                 gridsearch = False, cv=5):
        #print ('Baseline')
        self.verbose = verbose
        self.use_coarse_labels = coarse_labels
        self.gridsearch = gridsearch
        self.outputfile=output_file
        if model == 'forest':
            parameters = {'n_estimators': [10, 25, 50, 100], 'max_features': ['log2', 'sqrt'], 'bootstrap':[True,False] }
            #parameters = {'n_estimators': [10, 50, 100], 'max_features': ['log2']}
            #{'n_estimators': [10, 25, 50, 100], 'max_features': ('sqrt', 'log2')}
                          #'bootstrap': [True, False]}
            self.cls = RandomForestClassifier(class_weight='balanced_subsample', random_state=RANDOMSEED)
        elif model == 'SVM':
            parameters = {'kernel': ('linear', 'rbf'), 'C': [1, 10]}
            self.cls = SVC(kernel='rbf', class_weight='balanced', random_state=RANDOMSEED)

        elif model == 'NB':
            parameters = {'alpha': [0, 0.5, 1.0, 2.0]} # smoothing parameter
            self.cls = MultinomialNB()

        elif model == 'logit':
            parameters = {'penalty': ('l1', 'l2'), 'C': [1, 10]}
            self.cls = LogisticRegression(class_weight='balanced', random_state=RANDOMSEED)

        elif model == 'MLP':
            parameters = {'hidden_layer_sizes': [50, 100, 200, 300], 'activation': ('logistic', 'relu', 'tanh'), 'alpha':[0.001, 0.0001, 0.01, 0.1]}
            self.cls = MLPClassifier(max_iter=1000, early_stopping=True)

        elif model == 'extra':
            parameters = {'n_estimators': [10, 25, 50, 100], 'max_features': ['log2', 'sqrt'],
                          'bootstrap': [True, False]}
            #parameters = {'n_estimators': [50], 'max_features': ['log2']}
            #parameters = {'n_estimators': [10, 50, 100], 'max_features': ['log2'] }
                #, 'bootstrap':[True, False]}
            self.cls = ExtraTreesClassifier(class_weight='balanced_subsample', random_state=RANDOMSEED)

        if self.gridsearch:
            print ('using grid search')
            # scoring = { 'AP':'average_precision', 'AUC': 'roc_auc', 'Accuracy':'accuracy', 'F1': 'f1'}
            scoring = None
            self.cls = GridSearchCV(estimator=self.cls, param_grid=parameters, scoring = scoring,
                                    cv = cv, n_jobs=-1, verbose=5,  refit='AP')

    def fit(self, X, y):
        if self.use_coarse_labels:
            y = self.transform_labels(y)

        self.cls.fit(X, y)
        if self.verbose:
            acc = self.cls.score(X, y)
            y_hat = self.cls.predict(X)
            print (classification_report(y, y_hat))
            print ('[BOWClassifier.py] training acc:', acc)
        return

    def predict(self, X, y=None):
        y_hat = self.cls.predict(X)

        if type(y) != type(None):
            if self.use_coarse_labels:
                y = self.transform_labels(y)

            if self.verbose:
                print(classification_report(y, y_hat)) #, target_names=['not ' + cat, cat])
                acc = self.cls.score(X, y)
                print ('[BOWClassifier.py] prediction acc:', acc)

        if self.outputfile:
            with open ( self.outputfile, 'w+' ) as f:
                for y_i in y_hat:
                    f.write(str(y_i) + '\n')

        return y_hat

    def transform_labels(self, labels):
        ret = []
        return ret


# creates a feature vector with POS-tags
class FeatureVectorizer():
    def __init__(self, tfidf_type='tf', input_type='content', min_df=1, max_df=0.90, max_ngrams=3, use_tags = ['V'],
                 vocab=None):
        self.RANDOM_SEED = RANDOMSEED

        def gen_feature_vectorizer(type, min_docFreq=10, max_docFreq=0.70, max_totFeatures=50000, vocab=None,
                               min_ngrams=1, max_ngrams=1, input_type='filename'):
            eng_stopwords = set(stopwords.words('english'))
            if type is 'tf':
                return CountVectorizer(input=input_type, stop_words=eng_stopwords, min_df=min_docFreq,
                                       ngram_range=(min_ngrams, max_ngrams),
                                       max_features=max_totFeatures, max_df=max_docFreq, vocabulary=vocab)
            if type is 'tfb':
                return CountVectorizer(input=input_type, stop_words=eng_stopwords, min_df=min_docFreq,
                                       max_features=max_totFeatures, max_df=max_docFreq, binary=True, vocabulary=vocab)
            else:
                return TfidfVectorizer(input=input_type, stop_words=eng_stopwords, min_df=min_docFreq,
                                       max_features=max_totFeatures, max_df=max_docFreq, vocabulary=vocab)


        self.vectorizer = gen_feature_vectorizer(tfidf_type, input_type=input_type, min_docFreq=min_df,
                                                          max_docFreq=max_df, max_ngrams=max_ngrams, vocab=vocab)
        self.pos_vectorizer = gen_feature_vectorizer(tfidf_type, input_type=input_type, min_docFreq=min_df,
                                                          max_docFreq=1.0, max_ngrams=1, vocab=vocab)

        self.pos_tags = use_tags

    # TODO: currently unsupported, need to add the CMU Tweet tagger to support tagging POS
    # given a list of tweets return a list of tagged tweets
    def tag_text(self, tweets_text):
        # clean_text = normalizeTextForTagger(text)
        # tags = tokenize(clean_text)

        #tagged_text = runtagger_parse(tweets_text)
        tagged_text = []
        tweets_str = ''
        for tweets in tagged_text:
            tweet_pos = ''
            for (word, pos, confidence) in tweets:
                if pos in self.pos_tags:
                    item = word.lower() + '_' + pos
                    tweet_pos += item + ' '
            tweets_str += tweet_pos.strip().replace('\n', ' ') + '\n'

        tweets_pos = tweets_str.strip().split('\n')
        return tweets_pos

    def fit(self, tweets_text, vect='text'):
        if vect == 'text':
            # print(tweets_text)
            self.vectorizer.fit(tweets_text)
        elif vect == 'pos':
            self.pos_vectorizer.fit(tweets_text)

        return

    def transform(self, tweets_text, vect = 'text'):
        if vect == 'text':
            return self.vectorizer.transform(tweets_text)
        elif vect == 'pos':
            return self.pos_vectorizer.transform(tweets_text)

        return


def tune_classifier(tr_file = '', dev_file='', vocab_file=None,
                    save_classifier=True,
                    outcome='content_behavior',
                    model='extra',
                    use_pos = False,
                    model_path='classifiers/behavior/',
                    results_file = None):

    tr_data = pd.read_csv(tr_file)
    # assuming the data is already preprocessed
    # X_tr = preprocess_text(tr_data['text'].values)
    X_tr = tr_data['text_clean'].values
    Y_tr = np.array(tr_data[outcome].values)

    if dev_file:
        dev_data = pd.read_csv(dev_file)
        X_dev =dev_data['text_clean']
        Y_dev = np.array(dev_data[outcome].values)

    vocab_idx = None
    # load vocab if necessary
    if vocab_file:
        with open (vocab_file, 'rb') as f:
            dl_data = dill.load(vocab_file)
        vocab_idx = dl_data['text'].vocab.stoi

    fv = FeatureVectorizer(vocab=vocab_idx)

    fv.fit(X_tr)
    X_hat_tr = fv.transform(X_tr)

    if use_pos:
        fv.fit(fv.tag_text(X_tr), vect='pos')
        X_pos = fv.transform(fv.tag_text(X_tr), vect='pos')
        X_hat_tr = hstack([X_hat_tr, X_pos])

    # tff = TFFeature(random_state=RANDOMSEED)
    # vectorizer = tff.gen_feature_vectorizer('tf', input_type='content', min_docFreq=1, max_docFreq=0.90,
    #                                         max_ngrams=3)
    # # ADD PART OF SPEACH TAGGING
    # X_hat_test = vectorizer.fit_transform(X_test)

    bowc_extra = BOWClassifier(model=model, verbose=True, output_file = None,
                               coarse_labels=False, class_weight='balanced', gridsearch = True, cv=5)

    bowc_extra.fit(X_hat_tr,Y_tr)

    if dev_file:
        Y_hat_dev = bowc_extra.predict(fv.transform(X_dev), Y_dev)

        if results_file:
            dev_data[outcome+'_pred'] = Y_hat_dev
            makedirs(os.path.dirname(model_path))
            dev_data.to_csv( os.path.join(model_path, results_file))

    if save_classifier:
        makedirs(os.path.dirname(model_path))
        with open(os.path.join(model_path, model+'_classifier_pos_'+str(use_pos)+'.pkl'), 'wb') as f:
            pickle.dump({'BOWClassifier':bowc_extra, 'FeatureVectorizer':fv }, f)

def pred_classifier(ts_file='', pkl_file = '', use_pos = True, results_file='', outcome='content_behavior', filetype='csv', report = True):

    # load the classifier
    with open(pkl_file, 'rb') as f:
        pkl_data = pickle.load(f)

    cls = pkl_data['BOWClassifier'].cls.best_estimator_
    fv = pkl_data['FeatureVectorizer']

    # two different file types, ids used for json only ( to be able to link back to original tweet)
    original_data = []
    if filetype =='csv':
        ts_data = pd.read_csv(ts_file)
        X_test = ts_data['text_clean'].values

    elif filetype == 'json':
        text=[]

        with open(ts_file) as f:
            for i, line in enumerate(f):
                twt = json.loads(line)
                original_data.append(twt)
                text.append(twt['message'])

        X_test = tokenize_tweets(text)

    X_hat_test = fv.transform(X_test)

    # if use_pos: # TODO: make sure that this works with json as well
    #     # fv.fit(fv.tag_text(handle_text['text'].values), vect='pos')
    #     X_pos = fv.transform(fv.tag_text(handle_text['text_clean'].values), vect='pos')
    #     X_hat_test = hstack([X_hat_test, X_pos])

    if filetype == 'json':
        keys = twt.keys()
        data_dict = {}
        for temp in original_data:
            for key in keys:
                if key not in data_dict:
                    data_dict[key] = []
                data_dict[key].append(temp[key])

        original_df = pd.DataFrame.from_dict(data_dict)
        ts_data = original_df



    y_hat = cls.predict(X_hat_test)
    if results_file:
        ts_data[outcome+'_pred'] = y_hat
        ts_data.to_csv(results_file)

    # else:
    #     for y in y_hat:
    #         print (y)

    # first check if outcome is availabe and then do the following
    if report:
        y = ts_data[outcome].values
        print(classification_report(y, y_hat))

    return X_test, y_hat



def main(model='extra', pkl_file = ''):
    with open(pkl_file, 'rb') as f:
        pkl_data = pickle.load(f)

    cls =  pkl_data['BOWClassifier'].cls
    print(cls)
    print(cls.best_params_)
    print(cls.best_estimator_)
    # print(cls.cv_results_.keys())
    print(cls.best_score_)
    print('mean_test_AP', cls.cv_results_['mean_test_AP'][cls.best_index_])
    print('std_test_AP', cls.cv_results_['std_test_AP'][cls.best_index_])
    print('mean_test_AUC', cls.cv_results_['mean_test_AUC'][cls.best_index_])
    print('std_test_AUC', cls.cv_results_['std_test_AUC'][cls.best_index_])
    print('mean_test_F1', cls.cv_results_['mean_test_F1'][cls.best_index_])
    print('std_test_F1', cls.cv_results_['std_test_F1'][cls.best_index_])
    print('mean_test_Accuracy', cls.cv_results_['mean_test_Accuracy'][cls.best_index_])
    print('std_test_Accuracy', cls.cv_results_['std_test_Accuracy'][cls.best_index_])

    print('best params:', cls.cv_results_['params'][cls.best_index_])
    classifier = cls.best_estimator_
    return

def parse_input():
    #TODO: add path2/from model output
    parser = argparse.ArgumentParser(description='Develop classifier and tune and post-process data.', prefix_chars='-+')
    parser.add_argument('--usePOS', dest='usePOS', action='store_true', help='using POS features.(when tuning)')
    parser.add_argument('--mode', dest='mode', default='', help='specify the mode: tune, report, predict')
    parser.add_argument('--model', dest='model', default='extra',
                        help='Model used to do classification. Options: SVM, forest, extra, NB, logit. Default: extra')
    parser.add_argument('--train', dest='tr_file', default='data/behavior_tweets_tr.csv',
                        help='Data used model training when tuning')
    parser.add_argument('--dev', dest='dev_file', default=None,
                        help='Data use for model evaluation (optional in tune)')
    parser.add_argument('--test', dest='ts_file', default=None,
                        help='Data use for prediction [required for mode:predict]')
    parser.add_argument('--resultsFile', dest='results_file', default=None,
                        help='File name of the output for the predictions model evaluation (optional)')
    parser.add_argument('--modelFile', dest='modelFile', default='extra_classifier_pos_False.pkl',
                        help='path to the pickle file containing the model.')
    parser.add_argument('--modelPath', dest='model_path', default='classifier_results/',
                        help='path to the pickle file containing the model.')
    parser.add_argument('--outcome', dest='outcome', default='', help='A human-annotated variable used to do the classification. (REQUIRED)')

    args = parser.parse_args()
    print(args)
    #print (args.data[0], args.zip_diagnosis, args.n_topics, args.min_df)
    return args


if __name__=='__main__':
    # for the keeling server...
    #tune_classifier(path2coodings='/data/keeling/a/amorale4/twitter-crawlers/ref/codings/')
    args = parse_input()
    if args.mode == 'tune':
        tune_classifier(tr_file=args.tr_file, dev_file=args.dev_file, model=args.model, use_pos=args.usePOS,
                        outcome=args.outcome, results_file= args.results_file, model_path=args.model_path)
    # elif args.mode == 'report':
    #     main(model=args.model, pkl_file = args.modelFile)
    elif args.mode == 'predict':
        pred_classifier(ts_file=args.ts_file, use_pos=args.usePOS, pkl_file = args.modelFile, results_file=args.results_file)

    else:
        print (args.mode, 'not a valid mode. Use --help option to see usage.')