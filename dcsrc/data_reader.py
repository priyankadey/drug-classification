import os
import pandas as pd

class DataReader:
    def __init__(self, tr_files = [], dev_files=[], ts_files = [], vocab_file=None,
                    outcome='content_behavior', data_path='data/', debug=False):
        self.tr_files = tr_files
        self.dev_files = dev_files
        self.ts_files = ts_files
        self.vocab_file = vocab_file
        self.path = data_path
        self.outcome_variable = outcome
        self.train_data = None
        self.dev_data = None
        self.test_data = None
        self.debug = debug

    #helper method to read labelled data from a list of files
    def read_from_files(self, list_of_files):
        dataset = []

        for i, some_file in enumerate(list_of_files):
            data = open(some_file).read().splitlines()
            df = pd.DataFrame(data, columns=['Tweet'])
            # assuming that the naming convention is as follows: 'current_usage_clean.csv'
            df['Category'] = '_'.join(some_file.split('_')[:-1])
            df['Target'] = i
            dataset.append(df)
        return pd.concat(dataset, ignore_index=True).drop_duplicates()

    # reading dataset from multiple files
    def read(self):
        dataset = []
        # read datasets
        if len([os.path.join(self.path, some_file) for i, some_file in enumerate(self.tr_files)]) > 0:
            self.train_data = self.read_from_files(
                [os.path.join(self.path, some_file) for i, some_file in enumerate(self.tr_files)])


        if len([os.path.join(self.path, some_file) for i, some_file in enumerate(self.dev_files)]) > 0:
            self.dev_data = self.read_from_files(
                [os.path.join(self.path, some_file) for i, some_file in enumerate(self.dev_files)])

        if len([os.path.join(self.path, some_file) for i, some_file in enumerate(self.ts_files)]) > 0:
            self.test_data = self.read_from_files(
                [os.path.join(self.path, some_file) for i, some_file in enumerate(self.ts_files)])

        if self.debug:
            print(self.train_data)
            print(self.dev_data)
            print(self.test_data)

    def get_train_text(self):
        return self.train_data['Tweet'].values

    def get_dev_text(self):
        return self.dev_data['Tweet'].values

    def get_test_text(self):
        return self.test_data['Tweet'].values

    def get_train_target(self):
        return self.train_data['Target'].values

    def get_dev_target(self):
        return self.dev_data['Target'].values

    def get_test_target(self):
        return self.test_data['Target'].values


def debug():
    dr = DataReader(data_path = 'data/', tr_files = ['current_testing.csv', 'past_testing.csv', 'possible_testing.csv'])
    dr.read()
    print(dr.train_data)

if __name__ == '__main__':
    debug()
