import dill
import numpy as np

from baseline_classifiers import BOWClassifier
from baseline_classifiers import FeatureVectorizer
from data_reader import DataReader

def main(model='extra', task_num = 1):
    # TODO: make all of the input params as command line args
    vocab_file=None
    verbose=True # prints the results of the classification
    output = None # if provided saves the output model

    # task 1
    if task_num == 1:
    	tr_files = ['total_news_training.csv', 'total_usage_training.csv', 'total_reactions_training.csv', 'total_others_training.csv']
    	ts_files = ['total_news_testing.csv', 'total_usage_testing.csv', 'total_reactions_testing.csv', 'total_others_testing.csv']

    # task 2
    else: 
    	tr_files = ['total_current_training.csv', 'total_past_training.csv', 'total_possible_training.csv']
    	ts_files = ['total_current_testing.csv', 'total_past_testing.csv', 'total_possible_testing.csv']

    dr = DataReader(tr_files = tr_files, ts_files = ts_files)
    dr.read()
    # model='extra'

    X_tr = dr.get_train_text()
    Y_tr = dr.get_train_target()


    vocab_idx = None
    # load vocab if necessary
    if vocab_file:
        with open(vocab_file, 'rb') as f:
            dl_data = dill.load(vocab_file)
        vocab_idx = dl_data['text'].vocab.stoi

    fv = FeatureVectorizer(vocab=vocab_idx)

    fv.fit(X_tr)
    X_hat_tr = fv.transform(X_tr)

    bowc_cls = BOWClassifier(model=model, verbose=verbose, output_file=None,
                               coarse_labels=False, class_weight='balanced', gridsearch=True, cv=5)

    bowc_cls.fit(X_hat_tr, Y_tr)

    X_ts = dr.get_test_text()
    Y_ts = dr.get_test_target()

    bowc_cls.predict(fv.transform(X_ts) , Y_ts)


    return


if __name__== '__main__':

    print("Task 1: Classifier for news, usage, reactions, and other")
    print("Task 2: Classifier for current, past, and possible intent to use")
    task = int(input("Enter a 1 to run task 1 and 2 to run task 2: "))

    print('Naive Bayes')
    main(model='NB', task_num = task)

    print('Random Forrest')
    main(model='forest', task_num = task)

    print('SVM')
    main(model='SVM', task_num = task)

    print('Logistic Regression')
    main(model='logit', task_num = task)

    print('Multi-Layered Perceptron')
    main(model='MLP', task_num = task)

    print('Extremely Randomized Trees')
    main(model='extra', task_num = task)
