import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import preprocessor as p 
import sys

p.set_options(p.OPT.URL, p.OPT.EMOJI, p.OPT.MENTION)

def clean_tweets(file_names):
	for f in file_names:
		new_f = f[:-4] + 'clean.csv'
		f_open = open(f, "r")
        	f_2 = open(new_f, "w+")
        	contents = f_open.readlines()
        	for line in contents:
                	line = line.replace("'", "")
                	line = line.replace("\"", "")
                	line = line.replace("\n", "")
                	line = line.replace("!", "")
               		line = line.replace(".", "")
                	line = line.replace(",", "")
                	line = line.replace("#", "")
                	line = line.replace(":", "")
                	line = line.replace(";", "")
                	line = line.replace("?", "")
                	line = p.clean(line)
                	f_2.write(line+"\n")
        	f_open.close()
        	f_2.close()	

def construct_training_testing_dfs(categories):
	training_df = pd.DataFrame()
	testing_df = pd.DataFrame()
	for c in categories:
    		data = c + '.csv'
    		tr_df = pd.DataFrame(open(training_file), columns = ['Tweet'])
    		tr_df['Category'] = c
    		tr_df['Target'] = count
    		training_df = training_df.append(tr_df)
    
    		te_df = pd.DataFrame(open(testing_file), columns = ['Tweet'])
    		te_df['Category'] = c
    		te_df['Target'] = count
    		testing_df = testing_df.append(te_df)
    	return (training_df, testing_df)

def generate_tfidf(training_df, testing_df):
	count_vect = CountVectorizer()
	X_train_counts = count_vect.fit_transform(training_df['Tweet'])
	tfidf_transformer = TfidfTransformer()
	X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
	return (count_vect, X_train_tfidf)

def transform_tfidf(tf_idf, count_vect):
	X_new_counts = count_vect.transform(testing_df['Tweet'])
	tfidf_transformer = TfidfTransformer()
	X_new_tfidf = tfidf_transformer.transform(X_new_counts)
	return X_new_tfidf

def run_nb(train, test):
	clf = MultinomialNB().fit(X_train_tfidf, training_df['Target'])
	count_vect, X_train_tfidf = generate_tfidf(train, test)
	X_new_tfidf = transform_tfidf(X_train_tfidf, count_vect)
	predicted = clf.predict(X_new_tfidf)
	return predicted

def get_stats(predicted, true):
	pass
	
def main():
	file_names = sys.argv[1:]
	clean_tweets(file_names)
	categories = []
	for f in file_names:
		categories.append(f.split("_")[0])
	train, test = construct_training_testing_dfs(categories)
	
	print("This python script will run baseline classifiers based on the inputted data")
	print("Enter NB to run the multinomial naive bayes model")	
	model = input()	
	if model == "NB":
		NB_predicted = run_nb(tf_idf, train)
		get_stats(NB_predicted)
''' 
TO DO:
implement the get_stats function
implement other baseline classifiers
'''
