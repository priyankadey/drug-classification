import preprocessor as p
import pandas as pd

p.set_options(p.OPT.URL, p.OPT.EMOJI, p.OPT.MENTION)

files = ['data/possible_intent.csv', 'data/current_usage.csv', 'data/past_usage.csv']
for f in files:
        new_f = f[:-4] + '_clean.csv'
        f_open = open(f, "r")
        f_2 = open(new_f, "w+")
        contents = f_open.readlines()
        for line in contents:
                line = line.replace("'", "")
                line = line.replace("\"", "")
                line = line.replace("\n", "")
                line = line.replace("!", "")
                line = line.replace(".", "")
                line = line.replace(",", "")
                line = line.replace("#", "")
                line = line.replace(":", "")
                line = line.replace(";", "")
                line = line.replace("?", "")
                line = p.clean(line)
                f_2.write(line+"\n")
        f_open.close()
        f_2.close()

dfs = []
files = ['data/possible_intent_clean.csv', 'data/current_usage_clean.csv', 'data/past_usage_clean.csv']
for f in files:
    f_open = open(f, "r")
    lines = f_open.readlines()
    words = []
    for line in lines:
        words.extend(list(set(line.split(" "))))
    ws = list(set(words))
    words = []
    for word in ws:
        words.append(word.lower())

    df = pd.DataFrame(index=words, columns=range(len(lines)))
    df = df.fillna(0)
    for i in range (len(lines)):
        for word in lines[i].split(" "):
            df.loc[word.lower(), i] += 1
    df['freq'] = df.sum(axis=1)
    df['occurence'] = df['freq']/len(lines)
    df = df.sort_values(by = "freq", ascending=False)
    df = df.drop_duplicates()
    dfs.append(df)
    f_open.close()

# we need to read the test.csv file and classify this data 
f = open("data/test.csv", "r")
test = f.readlines()
res = open("data/results.csv", "r")
results = res.readlines()
iter = 0
naive_res = []

for line in test:
        p_pi = 1
        p_cu = 1
        p_pu = 1
        wos = line.split(" ")
        for w in wos:
                if w in dfs[0].index:
                        p_pi *= dfs[0].loc[w, 'occurence']
                if w in dfs[1].index:
                        p_cu *= dfs[1].loc[w, 'occurence']
                if w in dfs[2].index:
                        p_pu *= dfs[2].loc[w, 'occurence']

        p_pi *= 0.46
        p_cu *= 0.3
        p_pu *= 0.24

#        print(line)
        if (p_pi > p_cu) and (p_pi > p_pu):
                naive_res.append("possible\n")
        elif (p_cu > p_pi) and (p_cu > p_pu):
                naive_res.append("current\n")
        else:
                naive_res.append("past\n")

print(naive_res)
print(results)

total_right = 0
cur_right = 0
past_right = 0
pos_right = 0
cur_classified = 0
pos_classified = 0
past_classified = 0

for i in range (len(naive_res)):
        if naive_res[i] == results[i]:
                total_right += 1

        if naive_res[i] == 'current\n':
                if (i < 5):
                        cur_right += 1
                cur_classified += 1
        elif naive_res[i] == 'possible\n':
                pos_classified += 1
                if (i < 15) and (i >= 5):
                        pos_right += 1
        else:
                past_classified += 1
                if (i >= 15):
                        past_right += 1
#print(cur_right)
print("Accuracy: ", total_right/len(results))
print("current usage accuracy: ", cur_right/5)
print("possible intent accuracy: ", pos_right/10)
print("past usage accuracy: ", past_right/8)