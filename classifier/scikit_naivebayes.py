import pandas as pd
from sklearn.feature_extraction.text import CountVectorizer
from sklearn.feature_extraction.text import TfidfTransformer
from sklearn.naive_bayes import MultinomialNB
import numpy as np

data = open('current_usage_clean.csv').read().splitlines()
df = pd.DataFrame(data, columns = ['Tweet'])
df['Category'] = 'current_usage'
df['Target'] = 1

data = open('past_usage_clean.csv').read().splitlines()
df2 = pd.DataFrame(data, columns = ['Tweet'])
df2['Category'] = 'past_usage'
df2['Target'] = 2

data = open('possible_intent_clean.csv').read().splitlines()
df3 = pd.DataFrame(data, columns = ['Tweet'])
df3['Category'] = 'possible_intent'
df3['Target'] = 3

df = df.append(df2)
df = df.append(df3)

count_vect = CountVectorizer()
X_train_counts = count_vect.fit_transform(df['Tweet'])

tf_transformer = TfidfTransformer(use_idf=False).fit(X_train_counts)
X_train_tf = tf_transformer.transform(X_train_counts)
X_train_tf.shape

tfidf_transformer = TfidfTransformer()
X_train_tfidf = tfidf_transformer.fit_transform(X_train_counts)
X_train_tfidf.shape

clf = MultinomialNB().fit(X_train_tfidf, df['Target'])

pos_cats = ['current', 'past', 'possible']
res = ['current', 'current', 'current', 'current', 'current', 'possible', 'possible', 'possible', 'possible', 'possible', 'possible', 'possible', 'possible', 'possible', 'possible', 'past', 'past', 'past', 'past', 'past', 'past', 'past', 'past']
docs_new = open('test.csv').read().splitlines()
X_new_counts = count_vect.transform(docs_new)
X_new_tfidf = tfidf_transformer.transform(X_new_counts)

predicted = clf.predict(X_new_tfidf)

for doc, category in zip(docs_new, predicted):
    print('%r => %s' % (doc, pos_cats[category-1]))
    
arr = []
for i in range (len(res)):
    if res[i] == pos_cats[predicted[i]-1]:
        arr.append(1)
    else:
        arr.append(0)
np.mean(arr) * 100

